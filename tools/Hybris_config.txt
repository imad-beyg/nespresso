HEAD
<meta property="og:url" content="FRIENDLY_URL" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:image" content="" />
<meta property="og:image:width" content="" />
<meta property="og:image:height" content="" />
<meta property="fb:app_id" content="1727251284175615" />

<link rel="preload" href="https://www.nespresso.com/shared_res/agility/commons/fonts/NespressoLucas-XtraBd.woff" as="font" type="font/woff" crossorigin>
<link rel="preload" href="https://www.nespresso.com/shared_res/agility/commons/fonts/NespressoLucas-Regular.woff" as="font" type="font/woff" crossorigin>
<link rel="preload" href="https://www.nespresso.com/shared_res/agility/commons/fonts/NespressoLucas-Light.woff" as="font" type="font/woff" crossorigin>
<link rel="preload" href="https://www.nespresso.com/shared_res/agility/commons/fonts/NespressoLucas-Medium.woff" as="font" type="font/woff" crossorigin>
<link rel="preload" href="https://www.nespresso.com/shared_res/agility/commons/fonts/NespressoLucas-Bold.woff" as="font" type="font/woff" crossorigin>
<link rel="preload" href="https://www.nespresso.com/shared_res/agility/commons/fonts/NespressoLucas-SemiBd.woff" as="font" type="font/woff" crossorigin>

<link rel="preload" href="https://www.nespresso.com/shared_res/agility/masterOrigin/masterOrigin/css/app.css" as="style">
<link rel="preload" href="https://www.nespresso.com/shared_res/agility/masterOrigin/masterOrigin/js/manifest.js" as="script">
<link rel="preload" href="https://www.nespresso.com/shared_res/agility/masterOrigin/masterOrigin/js/vendor.js" as="script">
<link rel="preload" href="https://www.nespresso.com/shared_res/agility/masterOrigin/masterOrigin/js/app.js" as="script">

<link href="https://www.nespresso.com/shared_res/agility/masterOrigin/masterOrigin/css/app.css" rel="stylesheet" />

BODY
<div id="agilityApp"></div>
<script type="text/javascript" src="https://www.nespresso.com/shared_res/agility/masterOrigin/masterOrigin/js/manifest.js"></script>
<script type="text/javascript" src="https://www.nespresso.com/shared_res/agility/masterOrigin/masterOrigin/js/vendor.js"></script>
<script type="text/javascript" src="https://www.nespresso.com/shared_res/agility/masterOrigin/masterOrigin/js/app.js"></script>
