# master-origin

> Nespresso campaign

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# serve the dynamic banner (main-banner + AppBanner.vue)
npm run dev --banner

# build for production with minification
npm run build

# build for production with minification and deploy
# it will upload the landing page and the dynamic banner (prefix with banner-)
npm run build --deploy

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
