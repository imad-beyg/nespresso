import 'assets/js/bootstrap'

import Vue from 'vue'
import App from './AppLanding'
import mixin from './assets/js/mixin'

// Libraries
import Landing from 'nespresso-library/src/core/landing_v2'
import { addClass, find } from 'gaspard'

// Nespresso components
import VueLanding from 'nespresso-components/landing/VueLanding'
import VueMedia from 'nespresso-components/media/VueMedia'
import AddToCart from 'nespresso-components/AddToCart'

// Gvue Components
import VueTracking from 'gvue/src/assets/js/directives/VueTracking'
import Zone from 'gvue/src/core/Zone'
import Heading from 'gvue/src/core/Heading'
import Paragraph from 'gvue/src/core/Paragraph'
import Visual from 'gvue/src/core/Visual'
import Cta from 'gvue/src/core/Cta'
import Breadcrumb from 'gvue/src/components/Breadcrumb'
import PopinCollection from 'gvue/src/components/PopinCollection'
import ProductHeader from 'gvue/src/components/PopinCollection/popin/rightCol/ProductHeader'

Vue.config.performance = process.env.NODE_ENV === 'development'
Vue.config.devtools = process.env.NODE_ENV === 'development'
Vue.config.productionTip = process.env.NODE_ENV === 'development'

Vue.mixin(mixin)
Vue.use(VueTracking, {
    trackingLandingLabel: 'Master Origin',
    mapSectionsName: {
        'range-description': 'Range description',
        'vl_mexico': 'Coffee Mexico VL',
        'vl_colombia': 'Coffee Colombia VL',
        'ol_indonesia': 'Coffee Indonesia OL',
        'ol_ethiopia': 'Coffee Ethiopia OL',
        'ol_colombia': 'Coffee Colombia OL',
        'ol_india': 'Coffee India OL',
        'ol_nicaragua': 'Coffee Nicaragua OL',
        // indonesia: 'Coffee Indonesia',
        // nicaragua: 'Coffee Nicaragua',
        // india: 'Coffee India',
        // colombia: 'Coffee Colombia',
        // ethiopia: 'Coffee Ethiopia',
        'machine-experience-detailsOl': 'Machine OL',
        'machine-experience-detailsVl': 'Machine VL',
        'machine-experience-products': 'Machine list',
        'accessory-experience-details': 'Accessory informations',
        'accessory-experience-products': 'Bottom -Accessories'
    }
})
Vue.use(VueLanding, {
    project: 'masterOrigin',
    landing: 'masterOrigin',
    trackingLabelPrefix: 'Master Origin',
    getApiDescription: false,
    skipMutationKeys: [
        'link',
        'link_mobile',
        'param',
        'capsules',
        'img',
        'brand',
        'tracking'
    ]
})
Vue.use(VueMedia, {
    breakpoints: {
        S: 767,
        L: 1920,
        XL: Infinity
    },
    getImgSuffix (breakpoint) {
        return `_${breakpoint.name.toUpperCase()}`
    }
})

Vue.component('AddToCart', AddToCart)
Vue.component('Zone', Zone)
Vue.component('Heading', Heading)
Vue.component('Paragraph', Paragraph)
Vue.component('Visual', Visual)
Vue.component('Cta', Cta)
Vue.component('Breadcrumb', Breadcrumb)
Vue.component('PopinCollection', PopinCollection)
Vue.component('ProductHeader', ProductHeader)

addClass(find('html')[0], 'g_agility')

/* eslint-disable no-new */
new Vue({
    el: '#agilityApp',
    render (h) {
        App.props = Landing.props
        return h(App, {
            props: App.props.reduce((previous, current) => {
                previous[current] = this.$el.getAttribute(current)
                return previous
            }, {})
        })
    }
})
