export default {
    computed: {
        logoSrc () {
            return { folder: 'logos', name: ~['CA'].indexOf(this.$landing.country) && ~['fr'].indexOf(this.$landing.language) ? 'logoFrench' : 'logo', ext: 'svg', suffix: false }
        },
        logoSrcPopin () {
            return { folder: 'logos', name: ~['CA'].indexOf(this.$landing.country) && ~['fr'].indexOf(this.$landing.language) ? 'logoFrenchPopin' : 'logoPopin', ext: 'svg', suffix: false }
        },
        fairtrade () {
            return { folder: 'logos', name: 'fairtrade', ext: 'png', disable: !~['vl_colombia', 'ol_indonesia'].indexOf(this.coffee.id) }
        }
    }
}
