import Query from "./Query";

export default {
    methods: {
        goToAnchor() {
            if (Query.get() != null) this.scrollToElement(Query.get());
        },
        scrollToElement(el) {
            let elementPosition = this.getPosition(document.getElementById(el));
            window.scrollTo(elementPosition.x, elementPosition.y);
        },
        convertAnchor(anchor) {
            let suffix = anchor.substr(anchor.length - 2, 2);
            let prefix = "";
            if (suffix === "VL") {
                prefix = "vl_";
                anchor = prefix + anchor.substr(0, anchor.length - 2);
            } else if (anchor === "mexico") {
                prefix = "vl_";
                anchor = prefix + anchor;
            } else if (
                anchor === "indonesia" ||
                anchor === "ethiopia" ||
                anchor === "colombia" ||
                anchor === "india" ||
                anchor === "nicaragua"
            ) {
                prefix = "ol_";
                anchor = prefix + anchor;
            } else if (
                anchor === "machines" &&
                !this.$json.machineExperience.detailsOl.hide
            ) {
                anchor = "machine-experience-detailsOl";
            } else if (
                anchor === "machines" &&
                this.$json.machineExperience.detailsOl.hide
            ) {
                anchor = "machine-experience-detailsVl";
            } else if (anchor === "accessories") {
                anchor = "accessory-experience-details";
            } else {
                return anchor;
            }
            // console.log('>>>>>>>>>>>>>>>> Anchor:', anchor)
            return anchor;
        },
        getPosition(el) {
            let x = 0;
            let y = 0;
            while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
                x += el.offsetLeft - el.scrollLeft;
                y += el.offsetTop - el.scrollTop;
                el = el.offsetParent;
            }
            return { x, y };
        }
    }
};
