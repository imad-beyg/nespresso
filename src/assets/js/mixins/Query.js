class Query {
    constructor() {
        const urlQuery = window.location.search;
        if (urlQuery) {
            const allQueries = urlQuery.split("?")[1].split("&");
            for (let q of allQueries) {
                if (q.indexOf("=") === -1) {
                    this.query = q;
                    break;
                }
            }
        }
    }
    get() {
        return this.query || null;
    }
}

export default new Query();
