export default {
    state: {
        recipes: []
    },
    setRecipes (recipes) {
        this.state.recipes = recipes
    },
    findRecipeById (id) {
        for (const recipe of this.state.recipes) {
            if (recipe.id === id) {
                return recipe
            }
        }
    }
}
