import 'assets/js/bootstrap'

import Vue from 'vue'
import App from './AppBanner'
import mixin from './assets/js/mixin'

// Libraries
import Landing from 'nespresso-library/src/core/landing_v2'

// Nespresso components
import VueLanding from 'nespresso-components/landing/VueLanding'
import VueMedia from 'nespresso-components/media/VueMedia'
import AddToCart from 'nespresso-components/AddToCart'
import VueTracking from 'gvue/src/assets/js/directives/VueTracking'

// Gvue Components
import Zone from 'gvue/src/core/Zone'
import Heading from 'gvue/src/core/Heading'
import Paragraph from 'gvue/src/core/Paragraph'
import Visual from 'gvue/src/core/Visual'
import Cta from 'gvue/src/core/Cta'

Vue.config.performance = process.env.NODE_ENV === 'development'
Vue.config.devtools = process.env.NODE_ENV === 'development'
Vue.config.productionTip = process.env.NODE_ENV === 'development'

Vue.mixin(mixin)
Vue.use(VueTracking, {
    trackingLandingLabel: 'Master Origin',
    mapSectionsName: {
        'range-description': 'Range description',
        'vl_mexico': 'Coffee Mexico VL',
        'vl_colombia': 'Coffee Colombia VL',
        'ol_indonesia': 'Coffee Indonesia OL',
        'ol_ethiopia': 'Coffee Ethiopia OL',
        'ol_colombia': 'Coffee Colombia OL',
        'ol_india': 'Coffee India OL',
        'ol_nicaragua': 'Coffee Nicaragua OL',
        // indonesia: 'Coffee Indonesia',
        // nicaragua: 'Coffee Nicaragua',
        // india: 'Coffee India',
        // colombia: 'Coffee Colombia',
        // ethiopia: 'Coffee Ethiopia',
        'machine-experience-detailsOl': 'Machine OL',
        'machine-experience-detailsVl': 'Machine VL',
        'machine-experience-products': 'Machine list',
        'accessory-experience-details': 'Accessory informations',
        'accessory-experience-products': 'Bottom -Accessories'
    }
})
Vue.use(VueLanding, {
    project: 'masterOrigin',
    landing: 'masterOrigin',
    trackingLabelPrefix: 'Master Origin',
    getApiDescription: false
})
Vue.use(VueMedia, {
    breakpoints: {
        S: 767,
        L: 1920,
        XL: Infinity
    },
    getImgSuffix (breakpoint) {
        return `_${breakpoint.name.toUpperCase()}`
    }
})

Vue.component('AddToCart', AddToCart)
Vue.component('Zone', Zone)
Vue.component('Heading', Heading)
Vue.component('Paragraph', Paragraph)
Vue.component('Visual', Visual)
Vue.component('Cta', Cta)

/* eslint-disable no-new */
// <script type="text/javascript" data-landing="masterOrigin" data-bannerId="slider-contentCarrouselBlock-item-0" src="https://www.nespresso.com/shared_res/agility/masterOrigin/masterOrigin/js/banner-app.js" id="moBannerScript"></script>
// <link href="https://www.nespresso.com/shared_res/agility/masterOrigin/masterOrigin/css/banner-app.css" rel="stylesheet" media="screen">
// new Vue({
//     el: '#agilityApp',
//     render (h) {
//         App.props = Landing.props
//         return h(App, {
//             props: App.props.reduce((previous, current) => {
//                 previous[current] = this.$el.getAttribute(current)
//                 return previous
//             }, {})
//         })
//     }
// })

const thisScript = document.getElementById('moBannerScript')
if (thisScript) {
    const bannerId = thisScript.getAttribute('data-bannerid') || false
    const landingAttr = thisScript.getAttribute('data-landing') || 'agilityApp'
    let checkBannerExist = setInterval(() => {
        if (document.querySelector(`div[data-promotion-item-id=${bannerId}]`) && bannerId) {
            document.querySelector(`div[data-promotion-item-id=${bannerId}]`).innerHTML = ''
            document.querySelector(`div[data-promotion-item-id=${bannerId}]`).appendChild(document.createElement('app'))
            document.querySelector(`div[data-promotion-item-id=${bannerId}] app`).setAttribute('landing', landingAttr)
            document.querySelector(`div[data-promotion-item-id=${bannerId}] app`).setAttribute('id', `${bannerId}_vue`)
            console.log('loaded banner')
            new Vue({
                el: `#${bannerId}_vue`,
                render (h) {
                    App.props = VueLanding.props
                    return h(App, {
                        props: VueLanding.props.reduce((previous, current) => {
                            previous[current] = this.$el.getAttribute(current)
                            return previous
                        }, {})
                    })
                }
            })
            clearInterval(checkBannerExist)
        }
    }, 100)
}
